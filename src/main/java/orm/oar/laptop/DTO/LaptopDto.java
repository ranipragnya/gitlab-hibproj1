package orm.oar.laptop.DTO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="laptop_info")
public class LaptopDto implements Serializable
{

	@Id
	@Column(name="l_id")
	private Integer l_id;
	@Column(name="l_Bname")
	private String l_Bname;
	@Column(name="l_price")
	private Double l_price;
	
	@Column(name="l_os")
	private String l_os;
	
	public Integer getId() {
		return l_id;
	}
	public void setId(Integer id) {
		this.l_id = id;
	}
	public String getL_name() {
		return l_Bname;
	}
	public void setL_Bname(String l_name) {
		this.l_Bname = l_name;
	}
	public Double getL_price() {
		return l_price;
	}
	public void setL_price(Double l_price) {
		this.l_price = l_price;
	}
	public String getL_os() {
		return l_os;
	}
	public void setL_os(String l_os) {
		this.l_os = l_os;
	}
	@Override
	public String toString() {
		return "LaptopDto [id=" + l_id + ", l_name=" + l_Bname + ", l_price=" + l_price + ", l_os=" + l_os + "]";
	}

}
