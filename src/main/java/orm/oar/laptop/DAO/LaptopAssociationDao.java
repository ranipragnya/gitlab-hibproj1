package orm.oar.laptop.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;



import orm.oar.laptop.DTO.LaptopDto;

public class LaptopAssociationDao 
{

	public void saveLaptopDetails(LaptopDto laptopDTO)
	{
		Configuration configuration=new Configuration().configure();		
		SessionFactory sessionfactory=configuration.buildSessionFactory();
		Session session=sessionfactory.openSession();
		Transaction transaction =session.beginTransaction();
		session.save(laptopDTO);
		transaction.commit();
	
	}

	public LaptopDto getLaptopDetailsById(Integer id) {
		Configuration configuration = new Configuration();
		configuration.configure();
		//configuration.addAnnotatedClass(LaptopDto.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		return (LaptopDto) session.get(LaptopDto.class, id);
	}

	public void updateLaptopInfoById(Integer id, String lbname ) {
		LaptopDto laptopDTO = getLaptopDetailsById(id);
		if(laptopDTO != null) {
			Configuration configuration = new Configuration();
			configuration.configure();
			//configuration.addAnnotatedClass(LaptopDto.class);
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			laptopDTO.setL_Bname(lbname);
			session.update(laptopDTO);
			transaction.commit();
		}else {
			System.out.println("laptop brand name Update failed");
		}
	
	
	}

}
