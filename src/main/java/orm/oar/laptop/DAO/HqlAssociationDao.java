package orm.oar.laptop.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import orm.oar.laptop.DTO.LaptopDto;


public class HqlAssociationDao 
{
	public List<LaptopDto> getLaptopDetails() 
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		//configuration.addAnnotatedClass(LaptopDto.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from LaptopDto";
		Query query = session.createQuery(hql);
		List<LaptopDto> list = query.list();
		return list;
	}
	
public LaptopDto getLaptopDrtailsByLBName(String lname) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from LaptopDto where l_Bname=:lname";
		Query query = session.createQuery(hql);
		query.setParameter("lname", lname );
		LaptopDto uniqueResult = (LaptopDto) query.uniqueResult();
		return uniqueResult;
	}
	
	public void updateOSBylbname(String lbname,String os) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="update LaptopDto set l_os=:l_os where l_Bname=:lbname";
		Query query = session.createQuery(hql);
		query.setParameter("l_os", os);
		query.setParameter("lbname", lbname);
		int updateRows = query.executeUpdate();
		transaction.commit();
		if(updateRows == 0) {
			System.out.println("Update Operation Failed");
			return;
		}
		System.out.println("Update Operation successfull");
	}
	
	public void deleteById(Integer id) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="delete LaptopDto where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		int updateRows = query.executeUpdate();
		transaction.commit();
		if(updateRows == 0) {
			System.out.println("Delete Operation Failed");
			return;
		}
		System.out.println("Delete Operation successfull");
	}


}
